package ru.tsc.vinokurov.tm.api.repository;

import ru.tsc.vinokurov.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
